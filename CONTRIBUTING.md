# How to Contribute

## What do I need to know?

If you'd like to make a contribution

### Tools

- the status review is written in [Markdown](https://daringfireball.net/projects/markdown/syntax)
- the document is tracked under a [git reporitory](https://git-scm.com/) on the [Bitbucket service](https://bitbucket.org). They make a very nice [graphical client](https://www.sourcetreeapp.com/) if the command line isn't your preference.
- for advanced users, this can be compiled into Docx, PDF or TeX using [Bash Scripts](https://en.wikipedia.org/wiki/Bash_(Unix_shell)) and [Pandoc](http://pandoc.org/)

### Content Guidelines

The report is written in the style of an academic paper. 

It is the obligation of the authors to cite relevant prior work. References are stored in 'shared/oss.bib'. 

## How do I make a contribution?

Never made an open source contribution before? Wondering how contributions work in the in our project? Here's a quick rundown!

1. Find an issue that you are interested in addressing or a feature that you would like to add.
2. Fork the repository associated with the issue to your local account. This means that you will have a copy of the repository under your-BitBucket-username/repository-name.
3. Clone the repository to your local machine using git clone https://bitbucket-username@bitbucket.org/bitbucket-username/opensource-tssg.git.
4. Create a new branch for your fix using git checkout -b branch-name-here.
5. Make the appropriate changes for the issue you are trying to address or the feature that you want to add.
6. Use git add insert-paths-of-changed-files-here to add the file contents of the changed files to the "snapshot" git uses to manage the state of the project, also known as the index.
7. Use git commit -m "Insert a short message of the changes made here" to store the contents of the index with a descriptive message.
8. Push the changes to the remote repository using git push origin branch-name-here.
9. Submit a pull request to the upstream repository.
10. Title the pull request with a short description of the changes made and the issue or bug number associated with your change. For example, you can title an issue like so "Added more log outputting to resolve #4352".
11. In the description of the pull request, explain the changes that you made, any issues you think exist with the pull request you made, and any questions you have for the maintainer. It's OK if your pull request is not perfect (no pull request is), the reviewer will be able to help you fix any problems and improve it!
12. Wait for the pull request to be reviewed by a maintainer.
13. Make changes to the pull request if the reviewing maintainer recommends them.
14. Celebrate your success after your pull request is merged!

## Where can I go for help?

Contact the maintainer, Bernard Butler<bbutler@tssg.org>.
