\documentclass[11pt]{article}
\usepackage{a4wide,graphics,url,lastpage,fink}
\usepackage{natbib}
\usepackage{fancyheadings}

\begin{document}
\title{Open sourcing CBNE (Project \texttt{Tinos})}
\author{B Butler, TSSG, WIT}
\maketitle
\thispagestyle{plain}
\tableofcontents
\pagestyle{fancy}
\lhead{Version 0.3}
\chead{}
\rhead{\textbf{CBNE} - project \emph{Tinos}}
\lfoot{\finkfile}
\cfoot{}
\rfoot{\thepage\ of \pageref{LastPage}}
%\setlength{\headrulewidth}{0.4pt}
%\setlength{\footrulewidth}{0.4pt}

\section{Background}
\subsection{Purpose of this document}
This working paper considers several important questions that any project needs to face when considering whether/how to open source its software outputs.  The working paper was originally written for software developed in 4WARD, but the intention is that it would also be applicable to other projects in future.
 
\subsection{The motivating application}
WIT-TSSG developed CBNE (Component-Based Networking Environment) to assist researchers create virtual network architectures (where every component exists as software) in the form of an overlay network over a traditional (hardware- \emph{and} software)-based network.  CBNE can be used to simulate new network architectures, such as those that might be proposed in the case of a clean-slate internet.

The project has working title of \emph{\texttt{Tinos}}.

The reasons for open sourcing \texttt{Tinos}, in decreasing order of significance, are
\begin{enumerate}
\item Releasing the work as open source enables WIT-TSSG to engage with the network protocol research community, by providing a tool to support that community's research, thereby enhancing WIT-TSSG's reputation
\item It increases the visibility of this work in the 4WARD project, especially to 4WARD stakeholders
\item The \texttt{Tinos} open source community will provide feedback in the form of bug reports and enhancement requests, and eventually software of their own
\item It is also a means of gauging the potential of \texttt{Tinos}, i.e., how useful is it?
\end{enumerate}
We note that the most important requirement for WIT-TSSG is promoting and leading an active open source community based on the software.  This requirement for a vibrant open source community project is used as a basis for our recommendations. 

This report considers three aspects of open source
\begin{enumerate}
\item Choice of licence
\item Building the open source community
\item Choice of hosting provider
\end{enumerate}

In practice, all of these are related but we consider them separately for reasons of convenience.  The report also considers typical risks of open source projects. 
\section{Choice of licence}
There are many open source licences.  All grant usage, modification and distribution rights from the copyright owner (the licensor) to the licensee.  However, these rights are granted with conditions.  The most permissive open source licence is the MIT (X11) licence which simply requires that the licence notice is included in all derived works, and absolves authors from liability. A two-clause BSD licence (also known as the ``FreeBSD'' licence) is essentially similar, but a three-clause BSD licence adds a statement that the authors do not (by implication) endorse derived works.  The original four-clause BSD licence also had a third clause (of four) that derived works needed to credit the University of California---the controversial ``advertising clause''.  The four-clause BSD licence was replaced in 1999 by the modified three-clause BSD licence.  The MIT- and both the three- and two-clause BSD licences are approved by the OSI as open source licences.

Proprietary software can use permissively-licenced open source software in its dependencies.  For example, Mac OS X is proprietary to Apple but is built upon lots of (permissively-licenced) BSD-licensed software.  Thus permissive licences are seen as conducive to future commercialisation activity.  Less permissive licences are frequently teamed with a more traditional commercial licence when companies wish to sell add-on services and support to corporate clients; this is called \emph{dual-licensing}.  Often customers need to pay for the rights to modify and redistribute software according to the terms of the commercial licence; use under the alternative GPL licence would be free of charge.

The Apache licence is based on the modified BSD licence, but granting and revoking patent rights is made more explicit, providing better protection to the copyright owner against litigation.  The intention was to protect the rights holder from claims by licencees, but this condition made the Apache licence incompatible with GPL v2, though it is compatible with GPLv3.  Apache v2 is still considered a permissive, non-copyleft licence.

By contrast, GPL v3 is a ``copyleft'' licence, meaning that derived works must satisfy GPL v3.  This requirement was included by the Free Software Foundation (created by Richard Stallman) as a means of increasing the availability of free software by \emph{viral} means.  Software released under a GPL licence can be dual-licensed with more permissive licences, but that introduces more complexity.

The Lesser GPL relaxes the full GPL to allow derived works to include proprietary software.  That is, the LGPL places copyleft restrictions on the program itself but does not apply these restrictions to other software that merely uses the program.  It is still supported by the FSF, but the full GPL is recommended where possible.  The Open Source Initiative (OSI) approves both permissive and copyleft open source licences but the FSF strongly advocates copyleft licences such as GPL and LGPL only.

Some open source hosting providers take a view on licences.  For example, OW2 favours LGPL.  If that licence is not used and another OSI-approved licence is proposed instead, the reasons for this must be explained when the project is submitted to ObjectWeb for approval.

Other noteworthy general purpose licences include the ISC licence, effectively a simplified BSD licence, favoured by the OpenBSD project.
 
The Research Director of WIT-TSSG reviewed open source licences in 2006 and recommended that WIT-TSSG should release more software as open source and strongly favoured the permissive modified BSD (three-clause) licence, if possible.
\subsection{Recommendation}
CBNE has the following parts
\begin{itemize}
\item JNode---a full operating system implemented in Java---particularly its networking components
\item OpenFire and Spark IM Server and Client components (smack.jar and smackx.jar) from Jive software - needed for their XMPP functionality
\item WIT-TSSG code which modifies the existing JNode classes and adds compatible classes to extend JNode's functionality
\item Springsource code, drawn from various projects, enabling WIT-TSSG to package its software as OSGi bundles
\item WIT-TSSG build files, based on \texttt{ant} and \texttt{ivy}, for assembling these components into a working network simulator
\end{itemize}
The software has dependencies on other components, notably the Springsource DM Server, to which these bundles are deployed.

JNode is licenced under LGPL.  Springsource creates OSGi bundles from numerous open source jars and rereleases them under an Apache Version 2 licence.  The Jive software components are Apache version 2 licensed. The build components are also Apache version 2 licensed. Springsource DM Server is dual-licensed under GPL v3 and a commercial licence.

As a general rule, the easiest way to proceed is to choose the least permissive (equivalently, most restrictive) licence in the bundle as the licence governing the overall bundle.  By that reckoning, the licence to choose is LGPL, because
\begin{itemize}
\item It subsumes the licence requirements of the more permissive Apache-licensed software used in all the other components
\item Provided the Springsource DM Server does not form part of the software distribution, its more stringent (full GPL) licence requirements are separate---it is merely a dependency of the distributed software and is used in an unmodified way by CBNE.  However this interpretation should be confirmed with the TTO, Dr Michael Whelan.
\item LGPL should be sufficiently permissive not to act as a deterrent to potential partners that have an aversion to strong copyleft licences.
\end{itemize}

Note that CBNE added some improvements/extensions to JNode itself, so these enhancements are covered by copyleft restrictions and so must be shared with the JNode community if CBNE is to be distributed in its own right.  However, the remaining software in the mix is not subject to copyleft restrictions and the overall licence can remain LGPLv3. 

\section{Building the open source community}
As stated in the introduction, building a vibrant open source community is essential.

Authors such as Martin Krafft (http://phd.martin-krafft.net/) have reviewed how some packages become members of the debian package repository.  He used a Delphi approach to identify 24 factors (`influences') that make it more likely that an open source package will make it into the next release of debian.  They can be summarised as follows: the package must meet a need, fit well with other packages, be easy to use and maintain.

Popular books have been written on the subject of building communities, not just around software, notably \emph{The Art of Community} by Jono Bacon \citep{Bacon2009}, who leads Ubuntu's community efforts as Canonical Ltd's Community Manager.  This book contains lots of interesting anecdotes and practical advice.  The main theme is that it is necessary to ensure that community is attractive to newcomers and provides ongoing value to all its members and the book advises on how to achieve these objectives.

\citet{1121560} has similar views on community, but also has practical advice for open source software projects in particular, mentioning specific software tools and development practices.

At the University of Oxford, OSS Watch (www.oss-watch.ac.uk/) publishes a monthly newsletter and hosts a mailing list with informative articles on open source development, licencing and communities.  It is largely directed at the educational sector, but much of the articles are applicable to open source development more generally.  OSS Watch provides a good summary of IPR issues with software - copyright, assignment of copyright, licensing copyright, software patents, etc. One of the interesting distinctions they draw is between open and community source.  The difference lies in how the development project starts.  If the initial set of project members is restricted but is then opened out to the community largely to find bugs and possible new features, this is the community source model.  Arguably, this is closer to the proposed release strategy for \texttt{Tinos} than full open source where users and hackers are assumed to be more or less synonymous.

OSS Watch also notes the typical phases through which a successful open source project passes
\begin{description}
\item[incubation] Typically, a small number of developers start the project and seed it with an initial working version, probably with many features missing.
\item[growth] The project is announced to a larger group and new members are encouraged to join.  Typically, a \emph{benevolent dictator} leads the project at this stage and there is more focus on lowering the barriers to entry.
\item[maturity] A more democratic leadership takes over and decision making is by lazy consensus.  Issues are raised and closed at a steady rate.  The project becomes self-sustaining.
\item[decline] A steady state is reached; the code is archived and the community drifts away.  The software may continue in productive use for many years, but will become obsolete eventually.
\end{description} 

Unsuccessful projects fail to progress in this fashion.  Either the project is abandoned due to lack of community involvement, or the project is forked for technical or political reasons, adversely affecting both the original and spun-off project.  There are many reasons for open development projects to fail
\begin{itemize}
\item the project was ill-advised: it does not meet a need, there were IPR difficulties, or is not technically feasible
\item the community did not form and the original developers were either overwhelmed with support requests from users or disillusioned from lack of interest from others in their ``peer group''
\item the community that did form did not operate successfully, often for social reasons
\end{itemize}

Eric S Raymond, in two thought-provoking online essays (\emph{The Cathedral and the Bazaar} and \emph{Homesteading the Noosphere}) and collected in \citep{560911} describes the complex interplay between the open source community (``hackers''), open source development models and open source software itself.  He sees the success of open source development as being due to ``agile'' software development models and well-developed community structures providing non-financial rewards for talented hackers in that community.

In summary, some of the critical success factors are
\begin{itemize}
\item Provide leadership
  \begin{itemize}
  \item Develop a strategy in the form of a (published) product roadmap for \texttt{Tinos}
  \item Assign and resource a project owner
  \item Add new features in a continuous fashion (high activity level is good)---``Release early and release often''
  \item Announce all progress
  \item Always reply in a timely fashion to project members
  \item Provide support for new users
    \begin{itemize}
    \item Learn about problems, use cases, etc.
    \item Feed this back into the codebase
    \item Identify potential collaborations
    \item Keep your users satisfied
    \end{itemize}
  \item Measure success
    \begin{itemize}
    \item Define what constitutes success.  ESR has interesting insights on the hacker ``gift culture''.
    \item Use site analytics (reporting) tools
    \item Modify the strategy accordingly
    \item Investigate social networking tools such as ``Friend of a Friend'' networks
    \end{itemize}
  \end{itemize}
\item Promote the project
  \begin{itemize}
  \item Make the project easy to find by tagging, DOAP, etc.  Note that Description of a Project (DOAP) is an XML document that is used to share information about a project via external catalogues of open source software projects.
  \item Use blogging, microblogging (tweeting), feeds, etc.
  \item Announce new developments, releases, etc.  High activity levels attract a larger community.
  \item Identify the target audience (academics, leading industry commentators, etc.) for announcements
  \item Most internet searches for technical subjects place respected blogs high on the list of searches (they have large in-degree), so we will try to attract their attention
  \item Provide ``badges'' (project logos) that bloggers and project members can use to promote the project
  \item Participate in the Google \emph{Summer of Code} programme, both to gather attention and to attract students and their supervisors, with financial support from Google
  \end{itemize}
\item Lower the barriers to entry
  \begin{itemize}
  \item Register the project with the hosting provider, using keywords to make it easier to find
  \item Provide a version that can be deployed easily
  \item Provide white papers with scenario descriptions, given that this is a complex product
  \item Provide a wiki, so that project members can update documentation with their own observations
  \item Provide (links to) videos, screenshots, etc., to assist new users 
  \end{itemize}
\item Collect feedback from users
  \begin{itemize}
  \item Provide a forum for multi-way communication linked to the project wiki
  \item Collect quotations and real user stories
  \item Ask project members to link back to the project from their web pages, using badges, etc., to raise awareness
  \item Ask academic project members to cite the project in their papers
  \item Provide a formal bug/issue/feature tracker
  \end{itemize}
\item Have well-defined roles
  \begin{description}
  \item[Project owner/manager] has overall responsibility, with authority to make decisions such as accepting new features and approving releases, and to make major announcements
  \item[Designer/architect] oversees the overall technical direction of the project
  \item[Community representative] tracks bugs, feature requests, scenarios and user stories
  \item[Technical writer] ensures the documentation meets user needs and is kept up to date
  \item[Tester] looks for bugs and curates bug reports raised by the community - ensuring bugs can be replicated.
  \item[PR Officer] raises awareness of the project
  \item[Administrator] administers the project infrastructure (website and servers) and monitors the project according to the agreed project metrics
  \item[User] obtains the software, in either source or binary format, uses it in their job and provides feedback
  \item[Developer] fixes bugs and adds new features
  \end{description}
  Note that a person could have multiple roles, e.g., \emph{User} and \emph{Developer}, or \emph{Community Representative} and \emph{PR Officer}.  Alternatively, many people could share the same role, notably \emph{User}.  Such roles seems to be common across a number of open source projects.
\end{itemize}
\subsection{Recommendation}
Consider the individual recommendations above.
\section{Choice of hosting provider}
This is a complex decision, since the hosting provider supplies the infrastructure for achieving the goals of the project, and may also define constraints on acceptable licences, etc.

The first option to be considered is hosting the project at WIT-TSSG, where the Infrastructure Group has the physical infrastructure (servers) and can provide bandwidth via HEAnet.  The advantages are that \texttt{Tinos} is hosted at low cost, with few restrictions, and it would be easier to control.  The disadvantages are that the community infrastructure (content management system, bug tracking, forum, etc.) are not yet in place and the openness of ``hosting it yourself'' is questionable.  Therefore this option is discounted.

The remaining options are considered in the attached spreadsheet, which draws together research on open source hosting providers (notably from wikipedia and the websites of the hosting providers themselves) and attempts to provide a ``Which? Guide'' to open source project hosting providers.

By defining the following exclusions, we can limit the choice of hosting provider to a more manageable number.  We exclude those that are known not to offer
\begin{itemize}
\item bug tracking
\item a project wiki
\item up front cost (even for non-commercial projects), unless that cost is borne elsewhere by TSSG
\item 10,000 users or more
\item An Alexa rank of less than 60,000,000 (hence with moderate popularity at least)
\item Suitable for enterprise level software and not tied to a specific language (except Java), operating system, development methodology, etc
\end{itemize}

These restrictions define the basic rules for inclusion in the acceptable list.  The hosting providers that make the cut are:
\begin{description}
\item[Assembla (www.assembla.com)] offers shell access and personal branches.  It has moderate popularity, but a relatively high number of projects and users.  It does not offer a mailing list, so we would need to source one from elsewhere.  It is a ``cloud-based'' service.  It is free for open source projects and offers some community tools.  The main downside is that its project profile does not match our project and its brand recognition is low.

\item[BerliOS (www.berlios.de)], by contrast, has a better project profile.  There is a strong German flavour, as might be expected from its main sponsor.  Also, there is a danger that it might be too close to Fraunhofer Fokus, and many of the projects already have links to Fokus.  BerliOS uses a large number of portals but its interface is a little confusing.  It does not appear to have very strong community tools.

\item[GitHub] Gitgub does not support subversion (designed to use \texttt{git}, not \texttt{svn}).  It is reasonably well-featured and is growing rapidly because of its reputation as \emph{the} \texttt{git} site, given that \texttt{git} is seen as the ``coolest'' source control system (compared to cvs, subversion and even mercurial).

\item[Google code (code.google.com)] has good brand awareness and is easy to set up.  A project forum is not provided directly, however project wikis can be created and a Google Group can be formed to provide similar functionality.

\item[JavaForge (javaforge.com)] has a good mix of projects and reasonable specification, though it lacks shell access and private branches.  Forums and issue tracking look good.

\item[OSOR.eu (www.osor.eu)] is an EC-supported network of open source projects, derived from source forges from each of the Member and Associated States.  The intention is that the open source software should benefit e-government.  By its nature it is focused on communities and federation and there might be a political advantage to offer 4ward project outputs (funded by EC) in an EC-funded open source hosting provider.  However, it targets ``software to support public administration'', which might be problematic.

\item[The OW2 Consortium (formerly ObjectWeb) (www.ow2.org)] is particularly strong in relation to community issues.  The focus is on middleware, which is not quite the right sector.  However, it is closer than a site that primarily hosts end-user-facing software and web applications.  Since OW2 offers so much more than merely project hosting, membership is relatively expensive.  However, WIT-TSSG is a member so we may be able to get reduced fees.  Also, there is considerable emphasis on governance, with Technology, Ecosystems and Operations Councils.  Projects are vetted prior to acceptance.  OW2 has 3 Initiatives, of which the most relevant for CBNE might be the Embedded Systems initiative. 

\item[Project Kenai (kenai.com)] is offered by Sun to the open source (Java) community.  It has strong community features and a good mix of source code hosting features, though, as a new/beta site, it has relatively low ``brand recognition'' and user numbers.  Interestingly, the EU FP7 OPAALS project (of which the PCS Group in WIT-TSSG is a member) plans to host some of its software outputs there.

\item[Sourceforge (sourceforge.net)] is the best known and largest scale open source repository there is.  It hosting features are reasonably comprehensive and its recent revamp has improved its community/social networking features.  JNode was hosted there, as was software from the EU FP6 ONE project (of which the PCS Group in WIT-TSSG is a member).  Its size is both a strength and a weakness.  With its strong brand and large number of hosted products, Sourceforge is a ``safe'' option as a hosting provider.
\end{description}

We exclude BERLIOS, OSOR.eu and the OW2 consortium from the shortlist for more political reasons.  BERLIOS is very closely aligned with Fraunhofer Fokus and Germany - it serves almost as Germany's national sourceforge.  The profile of \texttt{Tinos} does not fit OSOR.eu, which has an e-government focus.  The OW2 consortium is still very much European focused and creates barriers to entry because of the need to pay membership fees.
 
The details of the resulting shortlist of six hosting providers is set out in Table~\ref{tab:ShortlistOfHostingProviders}. Other providers were considered and rejected for the reasons listed in Table~\ref{tab:ExcludedProviders}.

\subsection{Recommendation}
I suggest that Github, Google Code, Project Kenai and SourceForge all appear to meet \texttt{Tinos}'s needs.  A more detailed investigation of each of these four options would be needed to decide which to choose.

\section{Risks}
Where there is opportunity, there is usually also some risk.  Each risk is characterised by its likelihood of occurrence and by its cost to the project.  We note that costs come in different forms
\begin{itemize}
\item direct financial costs, e.g., being sued and needing to pay compensation
\item opportunite costs, if the project fails to achieve its potential
\item intangible costs, if the project damages WIT's reputation
\end{itemize}

Some of the risks are listed below
\begin{description}
\item[There is a problem with the software licences] We are assuming that all components are properly licensed.  However, if at least one of the software dependencies is not operating to the terms of its licences, it places our project in jeopardy.  To mitigate against this, we performed a full licence audit and did not need to identify alternative components.
\item[There is a problem with partners] Since WIT's work was funded by 4WARD, one of the other partners might object to open sourcing it, or wish to apply their own conditions.  To mitigate against this, WIT operate in a completely open manner and kept the 4WARD project informed of its plans and progress.  Indeed, the 4WARD project manager has requested that Tinos be open sourced as part of its outputs, consistent with the consortium agreement.
\item[Setting up the project proves more costly (generally, in terms of time) than expected] Creating an open source software project requires considerable effort.  The hosting provider can provide the basic infrastructure, but the project founder needs to ensure that a stable build is provided, with adequate documention and resources to enable reasonably experienced developers to install and configure \texttt{Tinos} and run sample scenarios.  To mitigate against this, the project founder needs to provide the resources to the main developers to do the job properly.
\item[There is little response from the wider open source community] Either there is not an ``itch to be scratched'' \citep{560911}, or the project was not ready due to lack of resources (see risk above), or the launch of the project failed to generate sufficient publicity and curiosity from the opeinion leaders in the peer group.  To mitigate against this, we need to do extensive market research, to ensure the project is valuable and to refine the promotion strategy.
\item[Initial response is good, but the momentum dies] Having created an online community, maintaining it is a big challenge.  To mitigate against loss of interest and motivation, we will draw upon the experience of other project founders who have been successful \citep{Bacon2009,1121560,560911}.  
\item[Hostile patches are submitted] One of the core tenets of open source is that everyone has the right to contribute to the source code. Hostile patches \citep{560911} have the potential to take a project in a different direction that might not be consistent with its core aims.  Sometimes this can cause rancour and division and wasted effort fighting internal battles. To mitigate against this, the project aims and terms of engagement should be very clear, and decision making should be transparent.  Disputes should be resolved as they arise, before they grow out of control.
\item[The project is forked] This is often a sign of a dysfunctional project.  While it can lead to healthy competition for a \emph{mature} project, it can be fatal to a project during its \emph{incubation} and \emph{growth} phases, with the main concerns being dilution of development effort and confusion for the user community.  Classic examples of (semi-)beneficial forking abound, c.f., Emacs versus XEmacs, ImageMagick versus GraphicsMagick, etc.  To mitigate against destructive forking, we need to get the community structures right.
\end{description}
\section{Acknowledgment}
Thanks are due to Jason Finnegan, M\'{i}che\'{a}l \'{O} Foghl\'{u}, Patsy Phelan, Miguel Ponce de Leon and Paul Watson (all from WIT-TSSG) for valuable input to this study.

\bibliographystyle{plainnat}
\bibliography{openSource}

\appendix
\begin{table}
\begin{tabular}{|p{2.5cm}|p{2cm}|p{2cm}|p{2cm}|p{2cm}|p{2cm}|p{2cm}|} \hline
servicename & Assembla & GitHub & Google Code & JavaForge & Project Kenai & SourceForge \\ \hline \hline
codehosting & Yes & Max 300MB on free plan & Yes & Yes & Yes (max 5) & Yes \\ \hline
codereview & No & Yes & Yes & No & No & No \\ \hline
bugtracking & Yes & Yes & Yes & Yes & Yes & Yes \\ \hline
wiki & Yes & Yes & Yes & Yes & Yes & Yes \\ \hline
mailinglist & No & No & No & No & Yes & Yes \\ \hline
forum & Yes & No & No & Yes & Yes & Yes \\ \hline
announce & Yes & No & No & Yes & No & Yes \\ \hline
buildsystem & Yes & No & No & Yes & No & No \\ \hline
others & FTP, Time Tracking, API & Public AP &  & Public remote API, Document management & Download area, IM Chatroom, public API, project website, extensive help, code can be hidden & Marketplace, Help Wanted, OpenID Relying Party, Download area \\ \hline
adfree & Yes & Yes & Yes & Yes & Yes & No \\ \hline
svn & Yes & No & Yes & Yes & Yes & Yes \\ \hline
git & Yes & Yes & No & No & Yes & Yes \\ \hline
onlinesupport & Yes & Yes & Yes & Yes & Yes & ? \\ \hline
manager & Assembla, LLC & Logical Awesome & Google & Intland Software & Sun Microsystems & GeekNet\\ \hline
establish &  & 2008-04 & 2006 & 2005 & 2008-10 & 1999-11 \\ \hline
t2\_notes & 200 MB Free. Ticket Tool. Wiki. Trac. & Git Hosting. Free for open source, paid otherwise. &  &  &  &  \\ \hline
users & 170000 & 100000 & &  & 33881 & 2650514\\ \hline
projects & 60,000+ & & &  & 6991 & 156141\\ \hline
prominentprojects & Gfire & Ruby on Rails & Google Gears &  & Sun Cloud APIs & Inkscape \\ \hline
alexarank & 13131131 & 2342342 & 1000000 &  & 57814814 & 198\\ \hline
t4\_notes & & & & Java projects only. Plugins for Eclipse and NetBeans & & \\ \hline
\end{tabular} \caption{Shortlist of Hosting Providers} \label{tab:ShortlistOfHostingProviders}
\end{table}

\begin{table}
\begin{tabular}{|l|p{11cm}|} \hline
Provider & Excluded because \\ \hline
Alioth & Preference for Debian-related projects \\
BetaVine & Linked to Vodafone \\
Bitbucket & Lacks a forum and subversion access \\
Codendi & Commercial (up-front cost) from Xerox---\texttt{Tinos} does not need support for software development processes \\
Codeplex & Well featured and with a good mix of projects, however it is supported by Micorsoft and most of the hosted projects are targeted towards the Microsoft platform. \\
Drupal & Only for Drupal-related projects \\
FreeDesktop & Intended for desktops using X.org window management \\
Freepository & offers a basic, no-frills hosting service but is lacking in community features. \\
Gitorious & Does not support subversion (designed to use \texttt{git}, not \texttt{svn}) \\
Gna! &  Only for FSF-recognized free software projects \\
GNU Savannah & mostly intended for GNU (FSF) projects, very poor Alexa ranking \\
GridyZone & Lacks a forum and other collaboration resources \\
KnowledgeForge & Favours OpenKnowledge projects; very few users \\
Launchpad & Favours bazaar rather than subversion; intended to support Ubuntu \\
LuaForge & Lua projects only\\
Mac OS Forge & Mac OS projects only\\
mozdev & Mozilla projects only\\
Openmoko & Openmoko-related projects only\\
openSUSE build service & Lacks a forum; specialised for building GNU/Linux distributions \\
Origo & Associated with ETH Zurich; low activity\\
RubyForge & Ruby projects only \\
SEUL.org & Focus on broadening GNU/Linux desktop apps and Free Software advocacy \\
tigris & Intended for software development tools, particularly those based on subversion \\
tuxfamily & very few users, concerns about its viability \\ \hline
\end{tabular} \caption{Excluded Hosting Providers} \label{tab:ExcludedProviders}
\end{table}

\end{document}