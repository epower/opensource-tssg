\documentclass[]{article}
\usepackage{lmodern}
\usepackage{amssymb,amsmath}
\usepackage{ifxetex,ifluatex}
\usepackage{fixltx2e} % provides \textsubscript
\ifnum 0\ifxetex 1\fi\ifluatex 1\fi=0 % if pdftex
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
\else % if luatex or xelatex
  \ifxetex
    \usepackage{mathspec}
  \else
    \usepackage{fontspec}
  \fi
  \defaultfontfeatures{Ligatures=TeX,Scale=MatchLowercase}
\fi
% use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
% use microtype if available
\IfFileExists{microtype.sty}{%
\usepackage{microtype}
\UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\usepackage[unicode=true]{hyperref}
\hypersetup{
            pdftitle={Open Source requirements and practices},
            pdfauthor={Open Source Study Group, TSSG},
            pdfborder={0 0 0},
            breaklinks=true}
\urlstyle{same}  % don't use monospace font for urls
\IfFileExists{parskip.sty}{%
\usepackage{parskip}
}{% else
\setlength{\parindent}{0pt}
\setlength{\parskip}{6pt plus 2pt minus 1pt}
}
\setlength{\emergencystretch}{3em}  % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{0}
% Redefines (sub)paragraphs to behave more like sections
\ifx\paragraph\undefined\else
\let\oldparagraph\paragraph
\renewcommand{\paragraph}[1]{\oldparagraph{#1}\mbox{}}
\fi
\ifx\subparagraph\undefined\else
\let\oldsubparagraph\subparagraph
\renewcommand{\subparagraph}[1]{\oldsubparagraph{#1}\mbox{}}
\fi

\title{Open Source requirements and practices}
\providecommand{\subtitle}[1]{}
\subtitle{Lessons for WIT/TSSG}
\author{Open Source Study Group, TSSG}
\date{5 August 2016}

\begin{document}
\maketitle

\section{Introduction}\label{introduction}

Many projects in TSSG produce open source software as outputs. In H2020
projects, it is assumed that the project outputs will include open
source software and open access publications. Open source software is
also a feature of basic research (SFI-funded) projects. EI projects can
also produce open source outputs.

Even if the project \emph{outputs} are not open source, it is highly
likely that, regardless of development language, the project
\emph{inputs} will include open source components. Some software
licences (typically those featuring \emph{copyleft} clauses) are not
compatible with subsequent use in close source. Failure to respect
licence conditions exposes WIT/TSSG to legal risks; ignorance of the law
is not an accepted defence!

When project projects are open source, community building is needed to
ensure that others contribute to the ongoing development of the
software. Not all contributors would be developers: end users can report
bugs and make feature requests and other community members can take on
other roles.

Furthermore, as an ICT research and technology organisation, open source
outputs need to be measured, to give a more rounded picture of our work.
WIT TTO asked what our outputs are, how we measure success (downloads,
merge requests, etc.). At present our ability to report even on
existing/recent projects seems limited, and the problem is even worse
with legacy open source outputs.

At the first meeting of our group, we agreed that there were gaps and
one of us (Bernard) offered to write the first draft of this discussion
document.

\section{Open source topics}\label{open-source-topics}

During the first meeting of the group, it was decided to consider the
following topics. ,

\subsection{Choice of licence}\label{choice-of-licence}

The choice of licence is not trivial, because some licences are not
compatible with each other. The good news is that good advice is
available. The bad news is that few developers in TSSG seem to be aware
of it. Therefore, we believe that

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  awareness needs to be raised (possibly via the Sofa Sessions or
  similar)
\item
  specific training needs to be offered
\end{enumerate}

One of the key considerations is how to audit a project's software
inputs and outputs, to ensure that the licence choice is based on
complete and reliable information.

\subsection{Choosing a repository}\label{choosing-a-repository}

Now that distributed VCS\footnote{VCS stands for version control system.}
such as \texttt{git} are ubiquitous, there is much greater freedom and
choice regarding the software repository used. While some projects might
mandate particular repositories, in practice the most likly choices are
github, butbucket (both 3rd party) and gitlab (hosted in-house). There
are pros and cons with each of these, and the choice is affected by
devops considerations more generally. Again, it is collecting knowledge
of good practices and disseminating this throughout the organisation.

\subsection{Encouraging involvement}\label{encouraging-involvement}

Open source software requires a community to ensure that it stays
``alive''. In practice, this means it needs a core group of
contributors/stewards at the very least. This requires time and effort,
possibly from TSSG staff. As it happens, Zeta is pursuing her PhD
studies in the area of open source community building, so she has expert
knowledge in this area.

\subsection{Measuring success}\label{measuring-success}

If the community is active, this is a good result and it should be
reported as such. Typically, commercial software repositories offer
per-project measurements of activity, so the challenge is to find ways
of aggregating them and deriving metrics that are meaningful in a TSSG
context.

\section{Resources}\label{resources}

Firstly, advice on open source in TSSG has been published before; most
of it is still relevant. For example, (Butler ()) documented the
reasoning used to decide how to open source the \texttt{tinos}
components developed by WIT-TSSG in the 4WARD project. This advice was
abstracted in (Butler, Crotty, et al. ()) to apply more generally to
projects, and references a case study (LinKoChuangLin2006) to make the
reasoning more concrete . More recently, email correspondence on the
\texttt{tech} mailing list highlighted the available of a) easy to use
websites to choose a suitable open source licence, b) the availability
of expertise in Red Hat (the largest company in the world that is based
on open source) and c) the licence adviser from the EU PROSE project,
see (Butler, Ó Foghlú, et al. ()) for an interesting email thread
drawing these three apects togeher. Sometimes, it might be possible to
use dual licensing, e.g., one licence is used for internal (WIT-TSSG)
use and another is used if it is offered to third parties (Blanco
(2012)).

Meanwhile there are companies like Rogue Wave (Cope (2014), Cope (2015))
and Sonatype (Jackson (2016)) offering services to reduce the technical
and legal risks arising from misusing open source software.

In relation to open source community building, motivation and
measurement of success, there is practical advice in (Bacon (2012),
Fogel (2005)) and more thoughful considerations in (Raymond (2001), Eric
S. Raymond (1999), Eric S Raymond (1999), Raymond (1998)).

\section{Next Steps}\label{next-steps}

The group should consider the topic and devise briefing/training
material for TSSG, gaining approval from senior management, and
investigate ways to ensure that best practices are followed.

\section*{References}\label{references}
\addcontentsline{toc}{section}{References}

\hypertarget{refs}{}
\hypertarget{ref-Bacon2012}{}
Bacon, J., 2012. The art of community: Building the new age of
participation. O'Reilly Media, Inc.

\hypertarget{ref-Blanco2012}{}
Blanco, E., 2012. Dual-licensing as a business model. URL
\url{http://oss-watch.ac.uk/resources/duallicence2}

\hypertarget{ref-Butler2010}{}
Butler, B., Open sourcing cbne (project \texttt{Tinos}).

\hypertarget{ref-ButlerCrottyPoncedeLeon2012}{}
Butler, B., Crotty, M., Ponce de Leon, M., Open sourcing a project.

\hypertarget{ref-ButlerOFoghluPoncedeLeon2016}{}
Butler, B., Ó Foghlú, M., Ponce de Leon, M., Email discussion.

\hypertarget{ref-Cope2015}{}
Cope, R., 2015. Open source support report. URL
\url{http://www.roguewave.com/programs/open-source-support-report}

\hypertarget{ref-Cope2014}{}
Cope, R., 2014. Reduce your open source security risk: Strategies, risks
and tools. URL
\url{http://www.roguewave.com/resources/white-papers/reduce-oss-risk-strategies-tools-tactics}

\hypertarget{ref-Fogel2005}{}
Fogel, K., 2005. Producing open source software: How to run a successful
free software project. O'Reilly Media, Inc.

\hypertarget{ref-Jackson2016}{}
Jackson, W., 2016. 2016 state of the software supply chain.

\hypertarget{ref-Raymond2001}{}
Raymond, E.S., 2001. The cathedral and the bazaar: Musings on linux and
open source by an accidental revolutionary. O'Reilly \& Associates,
Inc., Sebastopol, CA, USA.

\hypertarget{ref-Raymond1999}{}
Raymond, E.S., 1999. The magic cauldron, in: Proceedings of the 3rd
Annual Conference on Atlanta Linux Showcase - Volume 3, ALS'99. USENIX
Association, Berkeley, CA, USA, pp. 25--25.

\hypertarget{ref-Raymond1999a}{}
Raymond, E.S., 1999. Open sources: Voices from the open source
revolution, in:. O'Reilly Media, Inc., pp. 209--217.

\hypertarget{ref-Raymond1998}{}
Raymond, E.S., 1998. Homesteading the noosphere. First Monday 3.

\end{document}
