---
title: Open Source requirements and practices 
subtitle: Lessons for WIT/TSSG
author: Open Source Study Group, TSSG
date: 5 August 2016
---

# Introduction

Many projects in TSSG produce open source software as outputs. In H2020
projects, it is assumed that the project outputs will include open source
software and open access publications. Open source software is also a feature
of basic research (SFI-funded) projects. EI projects can also produce open
source outputs.

Even if the project *outputs* are not open source, it is highly likely that,
regardless of development language, the project *inputs* will include open
source components. Some software licences (typically those featuring *copyleft*
clauses) are not compatible with subsequent use in close source. Failure to
respect licence conditions exposes WIT/TSSG to legal risks; ignorance of the
law is not an accepted defence!

When project projects are open source, community building is needed to ensure
that others contribute to the ongoing development of the software. Not all
contributors would be developers: end users can report bugs and make feature
requests and other community members can take on other roles.

Furthermore, as an ICT research and technology organisation, open source
outputs need to be measured, to give a more rounded picture of our work. WIT
TTO asked what our outputs are, how we measure success (downloads, merge
requests, etc.).  At present our ability to report even on existing/recent
projects seems limited, and the problem is even worse with legacy open source
outputs.

At the first meeting of our group, we agreed that there were gaps and one of us
(Bernard) offered to write the first draft of this discussion document.

# Open source topics

During the first meeting of the group, it was decided to consider the following
topics.  , 

## Choice of licence

The choice of licence is not trivial, because some licences are not compatible
with each other.  The good news is that good advice is available. The bad news
is that few developers in TSSG seem to be aware of it. Therefore, we believe
that

1.   awareness needs to be raised (possibly via the Sofa Sessions or similar)
2.   specific training needs to be offered

One of the key considerations is how to audit a project's software inputs and
outputs, to ensure that the licence choice is based on complete and reliable
information.

## Choosing a repository

Now that distributed VCS[^1] such as `git` are ubiquitous, there is much
greater freedom and choice regarding the software repository used. While some
projects might mandate particular repositories, in practice the most likly
choices are github, butbucket (both 3rd party) and gitlab (hosted in-house).
There are pros and cons with each of these, and the choice is affected by
devops considerations more generally. Again, it is collecting knowledge of good
practices and disseminating this throughout the organisation.
  
## Encouraging involvement

Open source software requires a community to ensure that it stays "alive". In
practice, this means it needs a core group of contributors/stewards at the very
least. This requires time and effort, possibly from TSSG staff. As it happens,
Zeta is pursuing her PhD studies in the area of open source community building,
so she has expert knowledge in this area.

## Measuring success

If the community is active, this is a good result and it should be reported as
such. Typically, commercial software repositories offer per-project
measurements of activity, so the challenge is to find ways of aggregating them
and deriving metrics that are meaningful in a TSSG context.
 
# Resources

Firstly, advice on open source in TSSG has been published before; most of it is
still relevant. For example, (@Butler2010) documented the reasoning used to
decide how to open source the `tinos` components developed by WIT-TSSG in the
4WARD project.  This advice was abstracted in (@ButlerCrottyPoncedeLeon2012) to
apply more generally to projects, and references a case study
(LinKoChuangLin2006) to make the reasoning more concrete . More recently, email
correspondence on the `tech` mailing list highlighted the available of a) easy
to use websites to choose a suitable open source licence, b) the availability
of expertise in Red Hat (the largest company in the world that is based on open
source) and c) the licence adviser from the EU PROSE project, see
(@ButlerOFoghluPoncedeLeon2016) for an interesting email thread drawing these
three apects togeher. Sometimes, it might be possible to use dual licensing,
e.g., one licence is used for internal (WIT-TSSG) use and another is used if it
is offered to third parties (@Blanco2012).

Meanwhile there are companies like Rogue Wave (@Cope2014, @Cope2015) and
Sonatype (@Jackson2016) offering services to reduce the technical and legal
risks arising from misusing open source software.

In relation to open source community building, motivation and measurement of
success, there is practical advice in (@Bacon2012, @Fogel2005) and more
thoughful considerations in (@Raymond2001, @Raymond1999, @Raymond1999a,
@Raymond1998).

# Next Steps

The group should consider the topic and devise briefing/training material for
TSSG, gaining approval from senior management, and investigate ways to ensure
that best practices are followed.

[^1]: VCS stands for version control system.

# References

